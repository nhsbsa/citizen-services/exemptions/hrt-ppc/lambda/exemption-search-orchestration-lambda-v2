process.env.NODE_ENV = "local";
process.env.outBoundApiTimeout = "10000";
process.env.logApiRequestAndResponse = "true";
import { logger } from "@nhsbsa/health-charge-exemption-npm-logging";
import { GetCitizens } from "../src/get-citizens";
import { GetOtherCertificates } from "../src/get-other-certificates";
import {
  CertificateApi,
  CitizenApi,
  ExemptionServiceApi,
  GetCertificates,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const MAKE_REQUEST_METHOD_NAME = "makeRequest";
const FIRST_PAGE_METHOD_NAME = `${MAKE_REQUEST_METHOD_NAME}FirstPage`;
const ADDITIONAL_PAGE_METHOD_NAME = `${MAKE_REQUEST_METHOD_NAME}sAdditionalPages`;

export const winstonInfoLoggerSpy: jest.SpyInstance = jest.spyOn(
  logger,
  "info",
);

// citizen API calls
export const citizenApiSpyInstance: jest.SpyInstance = jest.spyOn(
  CitizenApi.prototype,
  MAKE_REQUEST_METHOD_NAME,
);
export const citizenRequestFirstPageSpy: jest.SpyInstance = jest.spyOn(
  GetCitizens.prototype,
  FIRST_PAGE_METHOD_NAME,
);
export const citizenRequestAdditionalPageSpy: jest.SpyInstance = jest.spyOn(
  GetCitizens.prototype,
  ADDITIONAL_PAGE_METHOD_NAME,
);

// certificate API calls
export const certificateApiSpyInstance: jest.SpyInstance = jest.spyOn(
  CertificateApi.prototype,
  MAKE_REQUEST_METHOD_NAME,
);
export const certificateRequestFirstPageSpy: jest.SpyInstance = jest.spyOn(
  GetCertificates.prototype,
  FIRST_PAGE_METHOD_NAME,
);
export const certificateRequestAdditionalPageSpy: jest.SpyInstance = jest.spyOn(
  GetCertificates.prototype,
  ADDITIONAL_PAGE_METHOD_NAME,
);
export const otherCertificateRequestPageSpy: jest.SpyInstance = jest.spyOn(
  GetOtherCertificates.prototype,
  `${MAKE_REQUEST_METHOD_NAME}SearchByPersonalDetails`,
);

// exemption service
export const exemptionServiceApiSpyInstance: jest.SpyInstance = jest.spyOn(
  ExemptionServiceApi.prototype,
  MAKE_REQUEST_METHOD_NAME,
);
