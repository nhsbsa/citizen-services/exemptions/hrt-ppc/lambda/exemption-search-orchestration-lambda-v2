import {
  mockParams,
  mockHeaders,
  mockOutgoingCallHeaders,
} from "../../__mocks__/mockData";
import { searchOrchestration } from "../../exemption-search-orchestration-lambda";
import {
  certificateApiResponseOnePage,
  citizenApiResponseMultiPageFirst,
  citizenApiResponseMultiPageSecond,
  citizenApiResponseMultiPageThird,
  successResponseMultiPageTwoCerts,
  otherCertificatesApiResponseMultiple,
  successResponseMultiPageTwoCertsWithMultiOthers,
  certificateApiResponseMultiPageFirst,
  certificateApiResponseMultiPageSecond,
  certificateApiResponseMultiPageThird,
  successResponseMultiPageFiftyThreeCerts,
  citizenApiResponseMultiPageFirst501Results,
  citizenApiResponseMultiPageSecond501Results,
  citizenApiResponseMultiPageThird501Results,
  citizenApiResponseMultiPageFourth501Results,
  citizenApiResponseMultiPageFith501Results,
  citizenApiResponseMultiPageSixth501Results,
  citizenApiResponseMultiPageSeventh501Results,
  citizenApiResponseMultiPageEighth501Results,
  citizenApiResponseMultiPageNinth501Results,
  citizenApiResponseMultiPageTenth501Results,
  citizenApiResponseMultiPageEleventh501Results,
} from "../../__mocks__";
import {
  certificateRequestFirstPageSpy,
  otherCertificateRequestPageSpy,
  citizenRequestAdditionalPageSpy,
  citizenRequestFirstPageSpy,
  certificateRequestAdditionalPageSpy,
} from "../../../test-setup/setup";
import {
  createCertificateResponseDaoMock,
  createCitizenResponseDaoMock,
  citizenIdsDelimited,
} from "../utils/test-functions";
import { expectedSizeInCertificateRequestBody } from "../utils/test-constants";
import {
  CertificatePagedResponse,
  CitizenPagedResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const CITIZEN_IDS_IN_CERTIFICATE_REQUEST_BODY =
  "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009l";
const expectedCertificateRequestBody = {
  citizenIds: CITIZEN_IDS_IN_CERTIFICATE_REQUEST_BODY,
  ...expectedSizeInCertificateRequestBody,
};

beforeEach(() => {
  certificateRequestFirstPageSpy.mockResolvedValue(
    createCertificateResponseDaoMock(certificateApiResponseOnePage),
  );
  otherCertificateRequestPageSpy.mockResolvedValue({ certificates: [] });
});

describe("search orchestration - multiple found", () => {
  it("multi-page success with 5 citizens and fourth citizen has 2 certificates, no 'other' certificates", async () => {
    // given
    const firstPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFirst);
    const secondPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageSecond);
    const thirdPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageThird);
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturn._embedded.citizens.concat(
        ...thirdPageMockReturn._embedded.citizens,
      ),
    );

    // when
    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseMultiPageTwoCerts);
  });

  it("multi-page success with 5 citizens and fourth citizen has 2 certificates, multiple other certificates", async () => {
    // given
    const firstPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFirst);
    const secondPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageSecond);
    const thirdPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageThird);
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturn._embedded.citizens.concat(
        ...thirdPageMockReturn._embedded.citizens,
      ),
    );
    otherCertificateRequestPageSpy.mockResolvedValue({
      certificates: otherCertificatesApiResponseMultiple,
    });

    // when
    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(
      successResponseMultiPageTwoCertsWithMultiOthers,
    );
  });

  it("multi-page success with 5 citizens, first has 52 certificates, third has 1 certificate, no 'other' certificates", async () => {
    // given
    const firstPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFirst);
    const secondPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageSecond);
    const thirdPageMockReturn: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageThird);
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturn._embedded.citizens.concat(
        thirdPageMockReturn._embedded.citizens,
      ),
    );
    const firstPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageFirst);
    const secondPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageSecond);
    const thirdPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageThird);

    certificateRequestFirstPageSpy.mockResolvedValue(firstPageMockReturnCert);
    certificateRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturnCert._embedded.certificates.concat(
        thirdPageMockReturnCert._embedded.certificates,
      ),
    );

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
      expectedCertificateRequestBody,
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseMultiPageFiftyThreeCerts);
  });

  it("multi-page success with 501 citizens, some have certificates, no other certificates", async () => {
    // given
    const firstPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFirst501Results);
    const secondPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageSecond501Results);
    const thirdPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageThird501Results);
    const fourthPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFourth501Results);
    const fithPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageFith501Results);
    const sixthPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageSixth501Results);
    const seventhPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(
        citizenApiResponseMultiPageSeventh501Results,
      );
    const eigthPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageEighth501Results);
    const ninthPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageNinth501Results);
    const tenthPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(citizenApiResponseMultiPageTenth501Results);
    const eleventhPageMockReturnCitizen: CitizenPagedResponse =
      createCitizenResponseDaoMock(
        citizenApiResponseMultiPageEleventh501Results,
      );

    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturnCitizen);
    citizenRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturnCitizen._embedded.citizens
        .concat(thirdPageMockReturnCitizen._embedded.citizens)
        .concat(fourthPageMockReturnCitizen._embedded.citizens)
        .concat(fithPageMockReturnCitizen._embedded.citizens)
        .concat(sixthPageMockReturnCitizen._embedded.citizens)
        .concat(seventhPageMockReturnCitizen._embedded.citizens)
        .concat(eigthPageMockReturnCitizen._embedded.citizens)
        .concat(ninthPageMockReturnCitizen._embedded.citizens)
        .concat(tenthPageMockReturnCitizen._embedded.citizens)
        .concat(eleventhPageMockReturnCitizen._embedded.citizens),
    );

    const firstPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageFirst);
    const secondPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageSecond);
    const thirdPageMockReturnCert: CertificatePagedResponse =
      createCertificateResponseDaoMock(certificateApiResponseMultiPageThird);

    certificateRequestFirstPageSpy.mockResolvedValue(firstPageMockReturnCert);
    certificateRequestAdditionalPageSpy.mockResolvedValue(
      secondPageMockReturnCert._embedded.certificates.concat(
        thirdPageMockReturnCert._embedded.certificates,
      ),
    );

    // when
    await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    const firstChunkCitizenIds = citizenIdsDelimited(
      firstPageMockReturnCitizen._embedded.citizens
        .concat(secondPageMockReturnCitizen._embedded.citizens)
        .concat(thirdPageMockReturnCitizen._embedded.citizens),
    );
    const secondChunkCitizenIds = citizenIdsDelimited(
      fourthPageMockReturnCitizen._embedded.citizens
        .concat(fithPageMockReturnCitizen._embedded.citizens)
        .concat(sixthPageMockReturnCitizen._embedded.citizens),
    );
    const thirdChunkCitizenIds = citizenIdsDelimited(
      seventhPageMockReturnCitizen._embedded.citizens
        .concat(eigthPageMockReturnCitizen._embedded.citizens)
        .concat(ninthPageMockReturnCitizen._embedded.citizens),
    );
    const fourthChunkCitizenIds = citizenIdsDelimited(
      tenthPageMockReturnCitizen._embedded.citizens.concat(
        eleventhPageMockReturnCitizen._embedded.citizens,
      ),
    );

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturnCitizen.page?.totalPages || 0,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(4);
    expect(certificateRequestFirstPageSpy).toHaveBeenNthCalledWith(
      1,
      {
        citizenIds: firstChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenNthCalledWith(
      2,
      {
        citizenIds: secondChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenNthCalledWith(
      3,
      {
        citizenIds: thirdChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenNthCalledWith(
      4,
      {
        citizenIds: fourthChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(4);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenNthCalledWith(
      1,
      {
        citizenIds: firstChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenNthCalledWith(
      2,
      {
        citizenIds: secondChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenNthCalledWith(
      3,
      {
        citizenIds: thirdChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenNthCalledWith(
      4,
      {
        citizenIds: fourthChunkCitizenIds,
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
  });
});
