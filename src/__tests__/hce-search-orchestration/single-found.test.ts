import {
  mockParams,
  mockHeaders,
  mockOutgoingCallHeaders,
  mockBasePartnerParams,
  mockPartnerSurnameParam,
  mockAllPartnerParams,
  mockPartnerPostcodeParam,
  mockPartnerDobParam,
  mockPartnerFirstNameParam,
} from "../../__mocks__/mockData";
import { searchOrchestration } from "../../exemption-search-orchestration-lambda";
import {
  certificateApiResponseOnePage,
  citizenApiResponseOnePage,
  successResponseTwoCerts,
  otherCertificatesApiResponseSingle,
  successResponseTwoCertsWithSingleOther,
  successResponseSinglePartnerCitizen,
  citizenApiResponseCitizenAndCitizenPartner,
  citizenApiResponsePartnerCitizenOnly,
  certificateApiResponseLisHc2,
  successResponseCitizenAndPartner,
} from "../../__mocks__";
import {
  certificateRequestFirstPageSpy,
  otherCertificateRequestPageSpy,
  citizenRequestFirstPageSpy,
  certificateRequestAdditionalPageSpy,
  winstonInfoLoggerSpy,
} from "../../../test-setup/setup";
import {
  createCertificateResponseDaoMock,
  createCitizenResponseDaoMock,
} from "../utils/test-functions";
import {
  expectedSizeInCertificateRequestBody,
  PAGE_SIZE,
  SERVICE_ENTERED_LOG_MESSAGE,
  SERVICE_SKIPPING_EXEMPTION_LOG_MESSAGE,
} from "../utils/test-constants";
import { assertLogger } from "@nhsbsa/health-charge-exemption-npm-common-test/dist/src/index";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-common-models";

beforeEach(() => {
  certificateRequestFirstPageSpy.mockResolvedValue(
    createCertificateResponseDaoMock(certificateApiResponseOnePage),
  );
  otherCertificateRequestPageSpy.mockResolvedValue({ certificates: [] });
});

describe("search orchestration - single result found", () => {
  it("single page response 2 citizens and first citizen has 2 certificates, no 'other' certificates", async () => {
    // given
    citizenRequestFirstPageSpy.mockResolvedValue(
      createCitizenResponseDaoMock(citizenApiResponseOnePage),
    );

    // when
    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseTwoCerts);
  });

  it("single page response 2 citizens and first citizen has 2 certificates, single 'other' certificate", async () => {
    // given
    citizenRequestFirstPageSpy.mockResolvedValue(
      createCitizenResponseDaoMock(citizenApiResponseOnePage),
    );
    otherCertificateRequestPageSpy.mockResolvedValue({
      certificates: otherCertificatesApiResponseSingle,
    });

    // then
    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    // when
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        ...expectedSizeInCertificateRequestBody,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseTwoCertsWithSingleOther);
  });

  it.each([
    [" NE15 8NY ", "NE158NY"],
    ["NE15 8NY", "NE158NY"],
    ["NE15     8NY", "NE158NY"],
    [" nE15   8Ny   ", "nE158Ny"],
  ])(
    "should trim spaces and return results when postcode is %s",
    async (inputPostcode: string, expectPostcode) => {
      // given
      const firstPageMockReturn = createCitizenResponseDaoMock(
        citizenApiResponseOnePage,
      );
      citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);

      // when
      const result = await searchOrchestration({
        params: { postcode: inputPostcode },
        headers: mockHeaders,
      });

      // then
      expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
      expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
      expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
        { postcode: expectPostcode, ...expectedSizeInCertificateRequestBody },
        mockOutgoingCallHeaders,
      );
      expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
      expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
        {
          citizenIds:
            "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
          ...expectedSizeInCertificateRequestBody,
        },
        mockOutgoingCallHeaders,
      );
      expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
      expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
        {
          citizenIds:
            "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
          ...expectedSizeInCertificateRequestBody,
        },
        mockOutgoingCallHeaders,
        certificateApiResponseOnePage.page.totalPages,
      );
      expect(result).toStrictEqual(successResponseTwoCerts);
    },
  );

  describe("search orchestration - single result found with partner citizen", () => {
    const outboundHeadersWithLisChannel = {
      ...mockOutgoingCallHeaders,
      channel: Channel.LIS_STAFF,
    };

    const expectedCertificateRequestBody = {
      citizenIds: "c0a800eb-90ee-1888-8190-ee82b6120003",
      ...expectedSizeInCertificateRequestBody,
    };

    it.each([
      mockPartnerFirstNameParam,
      mockPartnerSurnameParam,
      mockPartnerDobParam,
      mockPartnerPostcodeParam,
      mockBasePartnerParams, // first and last name
      {
        ...mockBasePartnerParams, // first and last name and dob
        ...mockPartnerDobParam,
      },
      {
        ...mockBasePartnerParams, // first and last name and postcode
        ...mockPartnerPostcodeParam,
      },
      {
        ...mockPartnerDobParam, // dob and postcode
        ...mockPartnerPostcodeParam,
      },
      {
        ...mockPartnerSurnameParam, // surname, dob and postcode
        ...mockPartnerDobParam,
        ...mockPartnerPostcodeParam,
      },
      {
        ...mockPartnerSurnameParam, // surname and dob
        ...mockPartnerDobParam,
      },
      {
        ...mockPartnerFirstNameParam, // firstname and dob
        ...mockPartnerDobParam,
      },
      {
        ...mockPartnerFirstNameParam, // firstname and postcode
        ...mockPartnerPostcodeParam,
      },
      {
        ...mockPartnerFirstNameParam, // firstname, dob and postcode
        ...mockPartnerDobParam,
        ...mockPartnerPostcodeParam,
      },
    ])(
      "single page response with single citizen and certificate when matching partner found %s",
      async (mockParams: object) => {
        // given
        const firstPageMockReturn = createCitizenResponseDaoMock(
          citizenApiResponsePartnerCitizenOnly,
        );
        citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
        certificateRequestFirstPageSpy.mockResolvedValue(
          createCertificateResponseDaoMock(certificateApiResponseLisHc2),
        );

        // when
        const result = await searchOrchestration({
          params: mockParams,
          headers: {
            ...mockHeaders,
            channel: Channel.LIS_STAFF,
          },
        });

        // then
        expect(result).toStrictEqual(successResponseSinglePartnerCitizen);
        expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
        expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
        expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
          mockParams,
          outboundHeadersWithLisChannel,
        );
        expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
        expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
          expectedCertificateRequestBody,
          outboundHeadersWithLisChannel,
        );
        expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
        expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
          expectedCertificateRequestBody,
          outboundHeadersWithLisChannel,
          citizenApiResponsePartnerCitizenOnly.page.size,
        );
        assertLogger(winstonInfoLoggerSpy, 5, [
          SERVICE_ENTERED_LOG_MESSAGE,
          "total of [1] citizens found across [1] pages",
          "Get certificates chunk [1] of total [1]",
          "total of [1] certificates found",
          SERVICE_SKIPPING_EXEMPTION_LOG_MESSAGE,
        ]);
      },
    );

    it("single page response with single citizen and certificate when matching partner found when all params provided", async () => {
      // given
      const firstPageMockReturn = createCitizenResponseDaoMock(
        citizenApiResponsePartnerCitizenOnly,
      );
      citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
      certificateRequestFirstPageSpy.mockResolvedValue(
        createCertificateResponseDaoMock(certificateApiResponseLisHc2),
      );

      // when
      const result = await searchOrchestration({
        params: mockAllPartnerParams,
        headers: {
          ...mockHeaders,
          channel: Channel.LIS_STAFF,
        },
      });

      // then
      expect(result).toStrictEqual(successResponseSinglePartnerCitizen);
      expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
      expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(
        mockAllPartnerParams,
      );
      expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
      expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
        mockAllPartnerParams,
        outboundHeadersWithLisChannel,
      );
      expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
      expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
        expectedCertificateRequestBody,
        outboundHeadersWithLisChannel,
      );
      expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
      expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
        expectedCertificateRequestBody,
        outboundHeadersWithLisChannel,
        1,
      );
      assertLogger(winstonInfoLoggerSpy, 4, [
        SERVICE_ENTERED_LOG_MESSAGE,
        "total of [1] citizens found across [1] pages",
        "Get certificates chunk [1] of total [1]",
        "total of [1] certificates found",
      ]);
    });

    it.each([
      mockPartnerFirstNameParam,
      mockPartnerSurnameParam,
      mockPartnerDobParam,
      mockPartnerPostcodeParam,
    ])(
      "single page response returning citizen, partner and the shared certificate when matching %s only",
      async (mockParams: object) => {
        // given
        const firstPageMockReturn = createCitizenResponseDaoMock(
          citizenApiResponseCitizenAndCitizenPartner,
        );
        citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
        certificateRequestFirstPageSpy.mockResolvedValue(
          createCertificateResponseDaoMock(certificateApiResponseLisHc2),
        );
        certificateRequestAdditionalPageSpy.mockResolvedValue([
          certificateApiResponseLisHc2,
        ]);
        const expectedCertificateCallBody = {
          ...expectedSizeInCertificateRequestBody,
          citizenIds:
            "0a004b01-8515-1a6c-8185-1558ae970000," +
            expectedCertificateRequestBody.citizenIds,
        };

        // when
        const result = await searchOrchestration({
          params: mockParams,
          headers: {
            ...mockHeaders,
            channel: Channel.LIS_STAFF,
          },
        });

        // then
        expect(result).toStrictEqual(successResponseCitizenAndPartner);
        expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
        expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
        expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
          mockParams,
          outboundHeadersWithLisChannel,
        );
        expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(1);
        expect(certificateRequestFirstPageSpy).toHaveBeenCalledWith(
          expectedCertificateCallBody,
          outboundHeadersWithLisChannel,
        );
        expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
        expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledWith(
          expectedCertificateCallBody,
          outboundHeadersWithLisChannel,
          1,
        );
        assertLogger(winstonInfoLoggerSpy, 5, [
          SERVICE_ENTERED_LOG_MESSAGE,
          "total of [2] citizens found across [1] pages",
          "Get certificates chunk [1] of total [1]",
          "total of [2] certificates found",
          SERVICE_SKIPPING_EXEMPTION_LOG_MESSAGE,
        ]);
      },
    );
  });
});
