import {
  mockParams,
  mockHeaders,
  mockOutgoingCallHeaders,
  mockParamsWithCertificateFilters,
} from "../../__mocks__/mockData";
import { searchOrchestration } from "../../exemption-search-orchestration-lambda";
import {
  certificateApiResponseOnePage,
  citizenApiResponseOnePage,
} from "../../__mocks__";
import {
  certificateRequestFirstPageSpy,
  otherCertificateRequestPageSpy,
  citizenRequestAdditionalPageSpy,
  citizenRequestFirstPageSpy,
  certificateRequestAdditionalPageSpy,
} from "../../../test-setup/setup";
import { createCertificateResponseDaoMock } from "../utils/test-functions";
import { CitizenPagedResponse } from "@nhsbsa/health-charge-exemption-npm-common-models";

const expectedNoExemptionResult = {
  healthChargeExemptions: {},
  otherExemptions: {},
};

beforeEach(() => {
  certificateRequestFirstPageSpy.mockResolvedValue(
    createCertificateResponseDaoMock(certificateApiResponseOnePage),
  );
  otherCertificateRequestPageSpy.mockResolvedValue({ certificates: [] });
});

describe("search orchestration - no exemptions found", () => {
  it("should respond an empty health charge exemptions list when no citizens found, no other exemptions certificates", async () => {
    // given
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(mockParams);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(0);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });

  it("should call certificate service with additional filter parameters, no other exemptions certificates", async () => {
    // given
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: mockParamsWithCertificateFilters,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(1);
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledWith(
      mockParamsWithCertificateFilters,
    );
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestFirstPageSpy).toHaveBeenCalledWith(
      mockParamsWithCertificateFilters,
      mockOutgoingCallHeaders,
    );
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledTimes(1);
    expect(citizenRequestAdditionalPageSpy).toHaveBeenCalledWith(
      mockParamsWithCertificateFilters,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateRequestFirstPageSpy).toHaveBeenCalledTimes(0);
    expect(certificateRequestAdditionalPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });

  it("should not make call to get other certificate types when firstName missing", async () => {
    // given
    const params = (({ firstName, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });

  it("should not make call to get other certificate types when lastName missing", async () => {
    // given
    const params = (({ lastName, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: {
        citizens: [],
      },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });

  it("should not make call to get other certificate types when dateOfBirth missing", async () => {
    // given
    const params = (({ dateOfBirth, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });

  it("should not make call to get other certificate types when postcode missing", async () => {
    // given
    const params = (({ postcode, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenPagedResponse = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenRequestFirstPageSpy.mockResolvedValue(firstPageMockReturn);
    citizenRequestAdditionalPageSpy.mockResolvedValue([]);

    // when
    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    // then
    expect(otherCertificateRequestPageSpy).toHaveBeenCalledTimes(0);
    expect(result).toStrictEqual(expectedNoExemptionResult);
  });
});
