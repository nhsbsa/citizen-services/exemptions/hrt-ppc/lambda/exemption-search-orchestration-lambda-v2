import {
  citizenApiResponseMultiPageFirst,
  citizenApiResponseMultiPageSecond,
  citizenApiResponseMultiPageThird,
  citizenApiResponseNoCitizens,
} from "../../__mocks__";
import {
  mockParams,
  mockHeaders,
  mockParamsWithPageOne,
} from "../../__mocks__/mockData";
import { GetCitizens } from "../../get-citizens";
import { citizenApiSpyInstance } from "../../../test-setup/setup";

const getCitizens = new GetCitizens();

describe("get citizens", () => {
  it("should make request and respond with the first page results", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(citizenApiResponseMultiPageFirst);

    // when
    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    // then
    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst._embedded.citizens,
      },
      _links: {
        first: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=0&size=2",
        },
        last: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=2&size=2",
        },
        next: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=1&size=2",
        },
        profile: {
          href: "http://localhost:8100/v1/profile/citizens",
        },
        search: {
          href: "http://localhost:8100/v1/citizens/search",
        },
        self: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&size=2&page=0",
        },
      },
      page: citizenApiResponseMultiPageFirst.page,
    };

    expect(citizenApiSpyInstance).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the empty when no citizens found ", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(citizenApiResponseNoCitizens);

    // when
    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    // then
    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseNoCitizens._embedded.citizens,
      },
      _links: {
        profile: {
          href: "http://localhost:8100/v1/profile/citizens",
        },
        search: {
          href: "http://localhost:8100/v1/citizens/search",
        },
        self: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1998-03-24&postcode=NE158NY",
        },
      },
      page: citizenApiResponseNoCitizens.page,
    };

    expect(citizenApiSpyInstance).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty citizens array and no page object when results undefined", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(undefined);

    // when
    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    // then
    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseNoCitizens._embedded.citizens,
      },
    };

    expect(citizenApiSpyInstance).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the second and third page results", async () => {
    // given
    citizenApiSpyInstance
      .mockResolvedValueOnce(citizenApiResponseMultiPageSecond)
      .mockResolvedValueOnce(citizenApiResponseMultiPageThird);
    const firstPage = citizenApiResponseMultiPageFirst.page;

    // when
    const result = await getCitizens.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    // then
    const expectCitizenApiCalledFirst = {
      method: "GET",
      url: "/v1/citizens?page=1&firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectCitizenApiCalledSecond = {
      ...expectCitizenApiCalledFirst,
      url: "/v1/citizens?page=2&firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
    };
    const expectedResult =
      citizenApiResponseMultiPageSecond._embedded.citizens.concat(
        citizenApiResponseMultiPageThird._embedded.citizens,
      );

    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      expectCitizenApiCalledFirst,
    );
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      expectCitizenApiCalledSecond,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty array when results undefined for additional pages", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue({
      page: 1,
      _embedded: { citizens: [] },
    });
    const firstPage = citizenApiResponseMultiPageFirst.page;

    // when
    const result = await getCitizens.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    // then
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(result).toStrictEqual([]);
  });
});
