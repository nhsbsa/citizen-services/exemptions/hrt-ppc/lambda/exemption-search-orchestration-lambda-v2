import { GetOtherCertificates } from "../../get-other-certificates";
import { exemptionServiceApiSpyInstance } from "../../../test-setup/setup";
import {
  exemptionServiceMultipleCertificate,
  exemptionServiceNoMatch,
  exemptionServiceSingleCertificate,
  expectedMultipleCertificateResponse,
  expectedSingleCertificateResponse,
} from "../../__mocks__";
import { mockParams } from "../../__mocks__/mockData";

const getOtherCertificates = new GetOtherCertificates();

const expectedRequestBody = {
  dateOfBirth: new Date(mockParams.dateOfBirth),
  familyName: mockParams.lastName,
  firstName: mockParams.firstName,
  postcode: mockParams.postcode,
};

describe("get other certificates", () => {
  it("should make request and respond with the results when single certificate found", async () => {
    // given
    exemptionServiceApiSpyInstance.mockResolvedValue(
      exemptionServiceSingleCertificate,
    );

    // when
    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    // then
    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HCE/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: expectedSingleCertificateResponse,
    };

    expect(exemptionServiceApiSpyInstance).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the results when multiple certificates found", async () => {
    // given
    exemptionServiceApiSpyInstance.mockResolvedValue(
      exemptionServiceMultipleCertificate,
    );

    // when
    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    // then
    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HCE/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: expectedMultipleCertificateResponse,
    };

    expect(exemptionServiceApiSpyInstance).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty when no certificates found", async () => {
    // given
    exemptionServiceApiSpyInstance.mockResolvedValue(exemptionServiceNoMatch);

    // when
    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    // then
    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HCE/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: [],
    };

    expect(exemptionServiceApiSpyInstance).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty certificates array when results undefined", async () => {
    // given
    exemptionServiceApiSpyInstance.mockResolvedValue(undefined);

    // when
    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    // then
    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HCE/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: [],
    };

    expect(exemptionServiceApiSpyInstance).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });
});
