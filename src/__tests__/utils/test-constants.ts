export const PAGE_SIZE = 500;
export const SERVICE_ENTERED_LOG_MESSAGE =
  "Entered exemption search orchestration service";
export const SERVICE_SKIPPING_EXEMPTION_LOG_MESSAGE =
  "Skipping call to exemption service as required params not provided";
export const expectedSizeInCertificateRequestBody = {
  size: PAGE_SIZE,
};
