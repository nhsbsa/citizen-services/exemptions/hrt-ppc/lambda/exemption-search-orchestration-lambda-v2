import {
  CertificatePagedResponse,
  CitizenGetResponse,
  CitizenPagedResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

export function createCertificateResponseDaoMock(
  certificateResponseMock,
): CertificatePagedResponse {
  return <CertificatePagedResponse>{
    _embedded: {
      certificates: certificateResponseMock._embedded.certificates,
    },
    page: certificateResponseMock.page,
  };
}

export function createCitizenResponseDaoMock(
  citizenResponseMock,
): CitizenPagedResponse {
  return {
    _embedded: {
      citizens: citizenResponseMock._embedded.citizens as CitizenGetResponse[],
    },
    page: citizenResponseMock.page,
  };
}

export function citizenIdsDelimited(
  citizens: Array<CitizenGetResponse>,
): string {
  const ids: Array<string> = [];
  for (const i in citizens) {
    ids.push(citizens[i].id);
  }
  return ids.join(",");
}
