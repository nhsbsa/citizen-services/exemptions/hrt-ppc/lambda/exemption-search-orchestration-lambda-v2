import { handler } from "../index";

import { searchOrchestration } from "../exemption-search-orchestration-lambda";
import successResponse from "../__mocks__/single-page-response.json";
import mockEvent from "../../events/event-all-fields.json";
import { APIGatewayEvent } from "aws-lambda";
import { Response } from "../interfaces";
import { winstonInfoLoggerSpy } from "../../test-setup/setup";
import { mockHeaders, mockParams } from "../__mocks__/mockData";

jest.mock("../exemption-search-orchestration-lambda");

let event: APIGatewayEvent;
const mockSearch = searchOrchestration as jest.MockedFunction<
  typeof searchOrchestration
>;

const badRequestBaseResponseBody = {
  status: 400,
  message: "There were validation issues with the request",
  timestamp: "2020-01-01T00:00:00.000Z",
  fieldErrors: [
    {
      field:
        "citizen.firstName, citizen.lastName, citizen.dateOfBirth, citizen.address.postcode",
      message: "At least one search parameter must not be null or empty",
    },
  ],
};

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEvent));
  mockSearch.mockClear();
  mockSearch.mockResolvedValue(successResponse as Response);
});

afterEach(() => {
  // then / expect these logs for all scenarios
  expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
    1,
    "exemption-search-orchestration-lambda",
  );
  expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
    2,
    "Request received [requestId: requestId][correlationId: e7fffacb-4575-47ba-b998-e0169945cdd5][userId: ABCD][channel: HRT_ONLINE]",
  );
});

describe("handler", () => {
  it("should respond with an bad request error if req body has no citizen", async () => {
    // given
    event.body = JSON.stringify({});

    // when
    const result = await handler(event);

    // then
    const expectedResponseBodyString = JSON.stringify(
      badRequestBaseResponseBody,
    );
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockSearch).toHaveBeenCalledTimes(0);
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it("should respond with an bad request error if request citizen fields are empty", async () => {
    // given
    event.body = JSON.stringify({
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      address: {
        postcode: "",
      },
    });

    // when
    const result = await handler(event);

    // then
    const expectedResponseBodyString = JSON.stringify(
      badRequestBaseResponseBody,
    );
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockSearch).toHaveBeenCalledTimes(0);
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it("should respond with an bad request error if event body has no citizen", async () => {
    // given
    event.body = JSON.stringify({});

    // when
    const result = await handler(event);

    // then
    const expectedResponseBodyString = JSON.stringify(
      badRequestBaseResponseBody,
    );
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockSearch).toHaveBeenCalledTimes(0);
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it("should respond with an internal server error", async () => {
    // given
    mockSearch.mockRejectedValueOnce(Error("Internal Server Error"));

    // when
    const result = await handler(event);

    // then
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
    expect(mockSearch).toHaveBeenCalledTimes(1);
    expect(mockSearch).toHaveBeenCalledWith({
      headers: mockHeaders,
      params: mockParams,
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
  });

  it("should respond with the 200 success response when citizens found", async () => {
    // given / when
    const result = await handler(event);

    // then
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(successResponse),
    });
    expect(mockSearch).toHaveBeenCalledTimes(1);
    expect(mockSearch).toHaveBeenCalledWith({
      headers: mockHeaders,
      params: mockParams,
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Request body validated",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      'Returning exemptions:{"healthChargeExemptions":{"citizens":[{"id":"0a004b01-8515-1a6c-8185-1558ae970000","firstName":"[REDACTED]","lastName":"[REDACTED]","dateOfBirth":"[REDACTED]","addresses":"[REDACTED]","certificates":[{"id":"c0a80005-8576-12ed-8185-77be4c060036","endDate":"2023-12-12","reference":"BB1D3E453CE002CC6512","startDate":"2022-12-12","status":"PENDING","type":"HRT_PPC"},{"id":"c0a80005-8576-12ed-8185-77be4f010037","endDate":"2023-12-12","reference":"E2CBB05CF07003447673","startDate":"2022-12-12","status":"PENDING","type":"HRT_PPC"}]},{"id":"0a004b01-8515-1a6c-8185-1558e1e10003","firstName":"[REDACTED]","lastName":"[REDACTED]","dateOfBirth":"[REDACTED]","addresses":"[REDACTED]","certificates":[]}]},"otherExemptions":{}}',
    );
  });
});
