export type SearchParams = {
  firstName?: string;
  lastName?: string;
  dateOfBirth?: string;
  postcode?: string;
  endDate?: string;
  startDate?: string;
  reference?: string;
  status?: string;
  page?: number;
  size?: number;
};

export type SearchParamsCertificate = {
  citizenIds: string;
  endDate?: string;
  startDate?: string;
  reference?: string;
  status?: string;
  page?: number;
  size?: number;
};
