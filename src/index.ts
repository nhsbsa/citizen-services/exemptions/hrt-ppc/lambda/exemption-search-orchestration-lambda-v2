import { APIGatewayEvent } from "aws-lambda";
import {
  getHeaderValue,
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/health-charge-exemption-npm-logging";
import { searchOrchestration } from "./exemption-search-orchestration-lambda";
import {
  successResponse,
  errorResponse,
  BadRequest,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import Redactyl from "redactyl.js";
import { Response } from "./interfaces";

const redactyl = new Redactyl({
  properties: ["firstName", "lastName", "dateOfBirth", "addresses"],
});

const logRequest = (headers, requestId) => {
  const correlationId = getHeaderValue(headers, "correlation-id");
  const channel = getHeaderValue(headers, "channel");
  const userId = getHeaderValue(headers, "user-id");

  loggerWithContext().info(
    `Request received [requestId: ${requestId}][correlationId: ${correlationId}][userId: ${userId}][channel: ${channel}]`,
  );
};

export async function handler(event: APIGatewayEvent) {
  const requestTime = new Date();
  try {
    loggerWithContext().info("exemption-search-orchestration-lambda");
    const {
      headers,
      body,
      requestContext: { requestId },
    } = event;

    setCorrelationId(getHeaderValue(headers, "correlation-id") as string);
    logRequest(headers, requestId);

    const payloadBody = JSON.parse(body || "{}");
    const { citizen = {}, certificate = {} } = payloadBody;
    const {
      firstName,
      lastName,
      dateOfBirth,
      address: { postcode = undefined } = {},
    } = citizen;
    const { endDate, startDate, reference, status } = certificate;

    if (!firstName && !lastName && !dateOfBirth && !postcode) {
      const message = "There were validation issues with the request";
      const fieldErrors = [
        {
          field:
            "citizen.firstName, citizen.lastName, citizen.dateOfBirth, citizen.address.postcode",
          message: "At least one search parameter must not be null or empty",
        },
      ];

      throw new BadRequest(message, requestTime, fieldErrors);
    }

    loggerWithContext().info("Request body validated");

    const params = {
      firstName,
      lastName,
      dateOfBirth,
      postcode,
      endDate,
      startDate,
      reference,
      status,
    };
    const result: Response = await searchOrchestration({ params, headers });
    loggerWithContext().info("Returning exemptions:" + JSON.stringify(redactyl.redact(result as any))); // eslint-disable-line
    return successResponse(200, result);
  } catch (err) {
    loggerWithContext().error({ message: err });
    return errorResponse(err);
  }
}
