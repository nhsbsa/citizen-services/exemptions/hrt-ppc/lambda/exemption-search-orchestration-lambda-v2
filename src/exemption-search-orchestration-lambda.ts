import { GetCitizens } from "./get-citizens";
import { GetOtherCertificates } from "./get-other-certificates";
import { isNil, omit } from "ramda";

import { RawAxiosRequestHeaders } from "axios";
import {
  GetCertificates,
  sliceArrayIntoChunks,
  transformHeadersAndReturnMandatory,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { SearchParams, SearchParamsCertificate } from "./types";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import { Citizen, Response } from "./interfaces";
import {
  Channel,
  CertificateResponse,
  CitizenGetResponse,
  CertificatePagedResponse,
  CitizenPagedResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const PAGE_SIZE = 500;

export async function searchOrchestration({
  params,
  headers,
}: {
  params: SearchParams;
  headers: RawAxiosRequestHeaders;
}) {
  params.size = PAGE_SIZE;
  loggerWithContext().info("Entered exemption search orchestration service");

  const callingServiceHeaders = transformHeadersAndReturnMandatory(headers);

  if (params.postcode) {
    params.postcode = params.postcode?.replace(/\s/g, "");
  }

  const { citizensTranspose, citizensResponse } = await getCitizens(
    params,
    callingServiceHeaders,
  );

  const citizenIds: Array<string> = citizensTranspose.map(({ id }) => id);

  const certificatesResponse = await getCertificates(
    params,
    callingServiceHeaders,
    citizenIds,
  );

  const otherExemptionsResponse = await getOtherExemptions(params);

  return buildResponse(
    otherExemptionsResponse,
    citizensResponse as CitizenGetResponse[],
    certificatesResponse ?? [],
  );
}

async function getCertificates(
  params: SearchParams,
  callingServiceHeaders: {
    "user-id": string;
    channel: Channel;
    "correlation-id": string;
  },
  citizenIds: Array<string>,
) {
  const url = "/v1/certificates/search/findByCitizens";
  const getCertificates = new GetCertificates(url);
  const certificatesTranspose: CertificateResponse[] = [];
  if (citizenIds.length > 0) {
    // Since we make a GET call the uri length is limited, therefore we chunk the citizen ids
    const chunkedCitizenIds: string[][] = sliceArrayIntoChunks(citizenIds, 150);
    for (let i = 0; i < chunkedCitizenIds.length; i++) {
      loggerWithContext().info(
        `Get certificates chunk [${i + 1}] of total [${
          chunkedCitizenIds.length
        }]`,
      );
      const searchParamsCertificate: SearchParamsCertificate = {
        citizenIds: chunkedCitizenIds[i].join(","),
        endDate: params.endDate,
        startDate: params.startDate,
        reference: params.reference,
        status: params.status,
        size: PAGE_SIZE,
      };
      // first page call to get the certificates and page info
      const firstPageCertificateDao: CertificatePagedResponse =
        await getCertificates.makeRequestFirstPage(
          searchParamsCertificate,
          callingServiceHeaders,
        );

      // additional page calls to collate the data from other pages
      const certificatesAdditionalPages =
        await getCertificates.makeRequestsAdditionalPages(
          searchParamsCertificate,
          callingServiceHeaders,
          firstPageCertificateDao.page?.totalPages || 0,
        );

      // add both to an array for later processing
      const certificatesResults: CertificateResponse[] = [
        ...firstPageCertificateDao._embedded.certificates,
        ...certificatesAdditionalPages,
      ];
      certificatesTranspose.push(...certificatesResults);
    }

    const certificatesResponse = certificatesTranspose.map(
      ({
        citizenId,
        id,
        reference,
        type,
        status,
        startDate,
        endDate,
        lowIncomeScheme,
      }) => ({
        id,
        citizenId,
        reference,
        type,
        status,
        startDate,
        endDate,
        lowIncomeScheme,
      }),
    );

    loggerWithContext().info(
      `total of [${certificatesResponse.length}] certificates found`,
    );
    return certificatesResponse;
  }
}

async function getCitizens(
  params: SearchParams,
  callingServiceHeaders: {
    "user-id": string;
    channel: Channel;
    "correlation-id": string;
  },
) {
  const getCitizens = new GetCitizens();

  // first page call to get the citizens and page info
  const firstCitizenPageDao: CitizenPagedResponse =
    await getCitizens.makeRequestFirstPage(params, callingServiceHeaders);

  // additional page calls to collate the data from other pages
  const citizensAdditionalPages = await getCitizens.makeRequestsAdditionalPages(
    params,
    callingServiceHeaders,
    firstCitizenPageDao.page?.totalPages || 0,
  );

  // add both to an array for later processing
  const citizensTranspose = [
    ...firstCitizenPageDao._embedded.citizens,
    ...citizensAdditionalPages,
  ];

  const citizensResponse = citizensTranspose.map(
    ({ id, firstName, lastName, dateOfBirth, addresses }) => ({
      id,
      firstName,
      lastName,
      dateOfBirth,
      addresses: addresses.map(
        ({
          id,
          type,
          addressLine1,
          addressLine2,
          townOrCity,
          country,
          postcode,
        }) => ({
          id,
          addressLine1,
          addressLine2,
          townOrCity,
          country,
          postcode,
          type,
        }),
      ),
    }),
  );

  loggerWithContext().info(
    `total of [${citizensResponse.length}] citizens found across [${firstCitizenPageDao.page?.totalPages}] pages`,
  );
  return { citizensTranspose, citizensResponse };
}

async function getOtherExemptions(params) {
  // Exemption service requires all params so only call if these are provided in incoming request
  if (
    !isNil(params.firstName) &&
    !isNil(params.lastName) &&
    !isNil(params.dateOfBirth) &&
    !isNil(params.postcode)
  ) {
    const getOtherCertificates = new GetOtherCertificates();

    const { certificates: exemptions } =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(params);

    const exemptionsResponse = {
      citizens: [
        {
          firstName: params.firstName,
          lastName: params.lastName,
          dateOfBirth: params.dateOfBirth,
          certificates: exemptions,
        },
      ],
    };

    return exemptionsResponse;
  }
  loggerWithContext().info(
    "Skipping call to exemption service as required params not provided",
  );
}

function buildResponse(
  otherExemptionsResponse,
  citizensResponse: CitizenGetResponse[],
  certificatesResponse?,
) {
  const response: Response = {};
  if (citizensResponse.length === 0) {
    response.healthChargeExemptions = {};
  } else {
    const citizensWithCertificates = citizensResponse.map(
      (citizen: CitizenGetResponse) => {
        const matchedCertificates = certificatesResponse
          .filter(
            (certificate: CertificateResponse) =>
              certificate.citizenId === citizen.id ||
              certificate.lowIncomeScheme?.partnerCitizenId === citizen.id,
          )
          .map((certificate: Partial<CertificateResponse>) =>
            omit(["citizenId", "lowIncomeScheme"], certificate),
          );

        return {
          ...citizen,
          certificates: matchedCertificates,
        };
      },
    );

    response.healthChargeExemptions = { citizens: citizensWithCertificates };
  }
  if (
    isNil(otherExemptionsResponse) ||
    (otherExemptionsResponse["citizens"] as { citizens: Citizen[] })[0]
      .certificates.length === 0
  ) {
    response.otherExemptions = {};
  } else {
    response.otherExemptions = otherExemptionsResponse;
  }

  return response;
}
