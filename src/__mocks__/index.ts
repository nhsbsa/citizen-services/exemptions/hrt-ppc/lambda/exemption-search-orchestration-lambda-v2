export { default as successResponseTwoCerts } from "../__mocks__/single-page-response.json";
export { default as successResponseTwoCertsWithSingleOther } from "../__mocks__/single-page-response-with-single-other.json";
export { default as successResponseMultiPageTwoCerts } from "../__mocks__/multi-page-response-2-certificates.json";
export { default as successResponseMultiPageTwoCertsWithMultiOthers } from "../__mocks__/multi-page-response-2-certificates-13-other.json";
export { default as successResponseMultiPageFiftyThreeCerts } from "../__mocks__/multi-page-response-53-certificates.json";
export { default as successResponseSinglePartnerCitizen } from "../__mocks__/single-page-response-single-citizen-partner.json";
export { default as successResponseCitizenAndPartner } from "../__mocks__/single-page-response-citizen-partner-match.json";

// citizen
export { default as citizenApiResponseOnePage } from "../__mocks__/api-responses/citizen-api/single-page-results.page-1.json";
export { default as citizenApiResponseNoCitizens } from "../__mocks__/api-responses/citizen-api/no-citizens-page-results.json";
export { default as citizenApiResponseMultiPageFirst } from "../__mocks__/api-responses/citizen-api/multi-page-results-page-1.json";
export { default as citizenApiResponseMultiPageSecond } from "../__mocks__/api-responses/citizen-api/multi-page-results-page-2.json";
export { default as citizenApiResponseMultiPageThird } from "../__mocks__/api-responses/citizen-api/multi-page-results-page-3.json";
export { default as citizenApiResponseMultiPageFirst501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-1.json";
export { default as citizenApiResponseMultiPageSecond501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-2.json";
export { default as citizenApiResponseMultiPageThird501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-3.json";
export { default as citizenApiResponseMultiPageFourth501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-4.json";
export { default as citizenApiResponseMultiPageFith501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-5.json";
export { default as citizenApiResponseMultiPageSixth501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-6.json";
export { default as citizenApiResponseMultiPageSeventh501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-7.json";
export { default as citizenApiResponseMultiPageEighth501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-8.json";
export { default as citizenApiResponseMultiPageNinth501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-9.json";
export { default as citizenApiResponseMultiPageTenth501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-10.json";
export { default as citizenApiResponseMultiPageEleventh501Results } from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-11.json";
export { default as citizenApiResponseCitizenAndCitizenPartner } from "../__mocks__/api-responses/citizen-api/single-page-with-partner-results.page-1.json";
export { default as citizenApiResponsePartnerCitizenOnly } from "../__mocks__/api-responses/citizen-api/single-page-with-partner-only-result.page-1.json";

// certificate
export { default as certificateApiResponseOnePage } from "../__mocks__/api-responses/certificate-api/single-page-results.page-1.json";
export { default as certificateApiResponseMultiPageFirst } from "../__mocks__/api-responses/certificate-api/multi-page-results-page-1.json";
export { default as certificateApiResponseMultiPageSecond } from "../__mocks__/api-responses/certificate-api/multi-page-results-page-2.json";
export { default as certificateApiResponseMultiPageThird } from "../__mocks__/api-responses/certificate-api/multi-page-results-page-3.json";
export { default as certificateApiResponseNoCertificates } from "../__mocks__/api-responses/certificate-api/no-certificates-page-results.json";
export { default as otherCertificatesApiResponseSingle } from "../__mocks__/expected-results/get-other-certificates/single-certificate-found.json";
export { default as otherCertificatesApiResponseMultiple } from "../__mocks__/expected-results/get-other-certificates/multiple-certificates-found.json";
export { default as certificateApiResponseLisHc2 } from "../__mocks__/api-responses/certificate-api/single-page-lis-result.json";

// exemption service
export { default as exemptionServiceSingleCertificate } from "../__mocks__/api-responses/exemption-service-api/single-certificate-found.json";
export { default as expectedSingleCertificateResponse } from "../__mocks__/expected-results/get-other-certificates/single-certificate-found.json";
export { default as exemptionServiceMultipleCertificate } from "../__mocks__/api-responses/exemption-service-api/multiple-certificates-found.json";
export { default as expectedMultipleCertificateResponse } from "../__mocks__/expected-results/get-other-certificates/multiple-certificates-found.json";
export { default as exemptionServiceNoMatch } from "../__mocks__/api-responses/exemption-service-api/no-certificates-page-results.json";
