export const mockParams = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
};

export const mockParamsWithCertificateFilters = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  endDate: "2022-03-24",
  startDate: "2023-03-24",
  reference: "ABCDE",
  status: "PENDING",
};

export const mockParamsWithPageOne = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  page: 1,
};

export const mockParamsWithPageTwo = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  page: 2,
};

export const mockPartnerFirstNameParam = {
  firstName: "Jane",
};

export const mockPartnerSurnameParam = {
  lastName: "Smith",
};

export const mockBasePartnerParams = {
  ...mockPartnerFirstNameParam,
  ...mockPartnerSurnameParam,
};

export const mockPartnerPostcodeParam = {
  postcode: "NE15 8NY",
};

export const mockPartnerDobParam = {
  dateOfBirth: "1998-11-19",
};

export const mockAllPartnerParams = {
  ...mockBasePartnerParams,
  ...mockPartnerDobParam,
  ...mockPartnerPostcodeParam,
};

export const mockHeaders = {
  "x-api-key": "x-api-key",
  channel: "HRT_ONLINE",
  "user-id": "ABCD",
  "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
};

export const mockOutgoingCallHeaders = {
  channel: "HRT_ONLINE",
  "user-id": "ABCD",
  "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
};
