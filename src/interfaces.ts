import {
  AddressResponse,
  CertificateStatus,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

export interface Citizen {
  id?: string;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  addresses?: AddressResponse[];
  certificates: Certificate[] | [];
}

export interface Certificate {
  id?: string;
  reference: string;
  type: string;
  startDate: string;
  endDate: string;
  status: CertificateStatus;
}

export interface Response {
  healthChargeExemptions?: { citizens: Citizen[] } | object;
  otherExemptions?: { citizens: Citizen[] } | object;
}
